.. Art98 documentation master file, created by
   sphinx-quickstart on Wed Jul 18 14:05:24 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Art98's documentation!
=================================

The main documentation for the site is organized into a couple sections:

* :ref:`dev-docs`
* :ref:`admin-docs`

.. _dev-docs:

.. toctree::
   :maxdepth: 2
   :caption: API Documentation

   api/index

.. _admin-docs:

.. toctree::
   :maxdepth: 2
   :caption: Admin Documentation
   
   artists
   artworks
   rfidauth
