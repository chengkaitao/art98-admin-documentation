Artists
=======
 
Adding Artists
--------------
 

Manually
********
1. On the |main_page|, Click on **Artists** under Backend. 

2. Click **Add Artist** on the right hand side of the screen.

3. Key in all fields, and click **Save**.

Import with CSV file
********************
1. On the |main_page|, Click on **Artists** under Backend.
2. Click on **Import** (Next to **Add Artist**) on the right hand side of the screen.
3. The file format **must be in CSV**. Choose the correct file to import.
4. Click **Submit**.
5. A preview will appear if the import is successful, then click **Import**.


Deleting Artists
----------------

1. On the |main_page|, Click on **Artists** under Backend.
2. On Artist page, tick the checkbox for the artists that you would want to delete.
3. Click on the **Action dropdown** located above the artist list table and pick **Delete Selected Artists**. Then click **Go**.
4. A confirmation with summary of artists that you want to delete will then pop up. Click **Yes, I'm sure** to permanently delete the artist(s) from the database.


.. |main_page| raw:: html

	<a href="http://art98-staging.herokuapp.com/admin/" target="_blank">Main Page</a>