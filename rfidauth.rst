Tagging Artwork using Android App
=================================

Logging in
----------

1. Using your phone, download and install the app |apk_url|

2. Connect the RFID Reader to your phone.

3. Open the app, login with credentials provided.

.. figure:: images/art98tag-login-page.jpeg
	:align: center

4. Upon succesful login, you will be redirected to the main page with two functions.

* :ref:`encode-tag`
* :ref:`verify-tag`


.. figure:: images/art98tag-main-page.jpeg
	:align: center

.. _encode-tag:

Encode Tag
----------

1. On the main page, click on the **Encode NFC** (top) button.

2. Select the artist and the artwork to tag.

.. figure:: images/art98tag-artist-list-page.jpeg
	:align: center

.. figure:: images/art98tag-artwork-list-page.jpeg
	:align: center

3. On this page, click on the blue button (with the magnifier and NFC). Once you see the loading animation, scan the tag using the RFID reader.

.. figure:: images/art98tag-encode-page.jpeg
	:align: center

4. If successful, you will see (Generating Security Certificate..) at the bottom of screen.

.. figure:: images/art98tag-generating-security-cert.jpeg
	:align: center


5. If successful, there will be an ok button on the bottom of screen. Press ok to complete the encoding process.

.. figure:: images/art98tag-encode-success.jpeg
	:align: center


.. _verify-tag:


Verify Tag
----------

1. On the main page, click on the **Green tick** (bottom) button.

2. Scan the tag using the RFID Reader.

Case 1
******
If tag is encoded (used) with artwork information, you will be able to see the artwork information, as shown in the picture below.

.. figure:: images/art98tag-verify-success.jpeg
	:align: center


Case 2
******
If the tag is not encoded (unused) with artwork information, you will see a verification fail which means the tag is not used.

.. figure:: images/art98tag-verify-fail.jpeg


.. |apk_url| raw:: html

	<a href="https://drive.google.com/file/d/15AxmvpCaVuf7EgMyvsEK0cMq5uQ6Q98O/view?usp=sharing">Here</a>