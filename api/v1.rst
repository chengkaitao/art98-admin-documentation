Art98 API uses :abbr:`REST (Representational State Transfer)`.
JSON is returned by all API responses including errors
and HTTP response status codes are to designate success and failure.

.. note::

	API is in early development stages.
	Currently supports:

	* Updating ArtworkRFID instances
	* URLs using mainly IDs instead of slug


Authentication and authorization
--------------------------------

Requests to the Art98 API are done using JWT Authentication with authentication endpoint provided.



.. http:post::  /api-token-auth/

	Exchanging username and password pair for a JWT Token

	:arg username: Required
	:arg password: Required

	**Example request**:

	.. sourcecode:: bash

		$ curl https://art98-staging.herokuapp.com/api-token-auth/ -d "username=admin&password=123456"

	**Example response**:

	.. sourcecode:: js

		{
			Token: "<JWT Token>"
		}

	:>json string token: JWT Token

Resources
---------

Artists
~~~~~~~~


Artists list
++++++++++++


.. http:get::  /api/artists/

	Retrieve a list of all Artists.

	**Example request**:

	.. sourcecode:: bash

		$ curl https://art98-staging.herokuapp.com/api/artists/

	**Example response**:

	.. sourcecode:: js

		[
		{
			"uuid":"a49faabe-d103-462c-97dd-d1c341ef255b",
			"created_by":"u2012",
			"name":"Lau Aik Huan",
			"created_at":"2018-05-22T06:35:00.173902Z"
			"dob":"1966-06-01",
			"image":null,
			"bio":"He is bad at drawing"
		},
		{
			"uuid":"a49faabe-d103-462c-97dd-d1c341ef255b",
			"created_by":"u4099",
			"name":"Joey Ng",
			"created_at":"2018-05-22T06:35:00.173902Z"
			"dob":"1906-05-04",
			"image":null,
			"bio":"Born in 1906"
		}
		]

	:>json uuid uuid: UUID of artist.
	:>json string created_by: User who created the artist.
	:>json string name: Name of artist.
	:>json datetime created_at: Timestamp at which artist is created.
	:>json date dob: Date of birth of artist.
	:>json string image: Image URL of artist.
	:>json string bio: Biography of artist.

	:statuscode 200: OK
	:statuscode 404: No artist available

Artist detail
+++++++++++++

.. http:get::  /api/artists/{uuid}/

	Retrieve details of a single artist.

	**Example request**:

	.. sourcecode:: bash

		$ curl https://art98-staging.herokuapp.com/api/artists/a49faabe-d103-462c-97dd-d1c341ef255b/

	**Example response**:

	.. sourcecode:: js

		[{
			"uuid":"a49faabe-d103-462c-97dd-d1c341ef255b",
			"created_by":"Lau Aik Huan",
			"name":"Lau Aik Huan",
			"created_at":"2018-05-22T06:35:00.173902Z"
			"dob":"1966-06-01",
			"image":null,
			"bio":"He is bad at drawing"
		}]

	:>json uuid uuid: UUID of artist.
	:>json string created_by: User who created the artist.
	:>json string name: Name of artist.
	:>json datetime created_at: Timestamp at which artist is created.
	:>json date dob: Date of birth of artist.
	:>json string image: Image URL of artist.
	:>json string bio: Biography of artist.

	:statuscode 200: OK
	:statuscode 404: There is no ``Artist`` with this UUID


Artworks
~~~~~~~~

Artwork List
++++++++++++


.. http:get::  /api/artworks/

	Retrieve a list of all Artworks.

	**Example request**:

	.. sourcecode:: bash

		$ curl https://art98-staging.herokuapp.com/api/artworks/

	**Example response**:

	.. sourcecode:: js

		[{
			"uuid":"a49faabe-d103-462c-97dd-d1c341ef255b",
			"created_by":"u9981",
			"serial_code":"123-test",
			"title":"Koi 1",
			"theme":"Koi",
			"medium":"Canvas",
			"year":"2016",
			"image":"http://www.google.com",
			"width":"1.01m",
			"height":"1.05m",
			"no_of_tags":"1",
			"created_at":"2018-05-22T06:35:00.173902Z"
			"status":"tagged",
			"artist":"a49faabe-d103-462c-97dd-d1c341ef255b"
		},
		{
			"uuid":"a49faabe-d103-462c-97dd-d1c341ef255b",
			"created_by":"u1093",
			"serial_code":"12166-test",
			"title":"Koi 2",
			"theme":"Koi",
			"medium":"Oil on Canvas",
			"year":"2015",
			"image":"http://www.amazon.com",
			"width":"2.01m",
			"height":"1.35m",
			"no_of_tags":"2",
			"created_at":"2018-05-22T06:35:00.173902Z"
			"status":"not tagged",
			"artist":"a49faabe-d103-462c-97dd-d1c341ef255b"
		}
		]

	:>json uuid uuid: UUID of artwork.
	:>json string created_by: User who created the artwork.
	:>json string serial_code: Serial code for artwork.
	:>json string title: Title of artwork.
	:>json string theme: Theme of artwork.
	:>json string medium: Medium of artwork.
	:>json integer year: Year of artwork.
	:>json string image: Image URL of artwork.
	:>json string width: Width of artwork.
	:>json string height: Height of artwork.
	:>json integer no_of_tags: No of tags required for the artwork.
	:>json datetime created_at: Timestamp at which artwork is created.
	:>json string status: Tag status of artwork.
	:>json foreignkey artist: Artist.


	:statuscode 200: OK
	:statuscode 404: No artwork is available

Artwork Detail
++++++++++++++


.. http:get::  /api/artworks/{uuid}/

	Retrieve details of a single artwork.

	**Example request**:

	.. sourcecode:: bash

		$ curl https://art98-staging.herokuapp.com/api/artworks/a49faabe-d103-462c-97dd-d1c341ef255b/

	**Example response**:

	.. sourcecode:: js

		[{
			"uuid":"a49faabe-d103-462c-97dd-d1c341ef255b",
			"created_by":"Lau Aik Huan",
			"serial_code":"123-test",
			"title":"Koi 1",
			"theme":"Koi",
			"medium":"Canvas",
			"year":"2016",
			"image":"http://www.google.com",
			"width":"1.01m",
			"height":"1.05m",
			"no_of_tags":"1",
			"created_at":"2018-05-22T06:35:00.173902Z"
			"status":"tagged",
			"artist":"a49faabe-d103-462c-97dd-d1c341ef255b"
		}
		]

	:>json uuid uuid: UUID of artwork.
	:>json string created_by: User who created the artwork.
	:>json string serial_code: Serial code for artwork.
	:>json string title: Title of artwork.
	:>json string theme: Theme of artwork.
	:>json string medium: Medium of artwork.
	:>json integer year: Year of artwork.
	:>json string image: Image URL of artwork.
	:>json string width: Width of artwork.
	:>json string height: Height of artwork.
	:>json integer no_of_tags: No of tags required for the artwork.
	:>json datetime created_at: Timestamp at which artwork is created.
	:>json string status: Tag status of artwork.
	:>json foreignkey artist: Artist.


	:statuscode 200: OK
	:statuscode 404: There is no ``Artwork`` with this UUID

Artwork RFID
~~~~~~~~~~~~

Tagging Artwork with POST
+++++++++++++++++++++++++

.. http:post::  /api/rfidauth/

	Endpoint to tag an artwork. Authorization with JWT Header is **Required**

	:arg string artwork_id: Required
	:arg array tag_id: Required
	:arg string tag_status: Required 

	**Example request**:

	.. sourcecode:: bash

		$ curl https://art98-staging.herokuapp.com/api/rfidauth/
		-d "artwork_id=a49faabe-d103-462c-97dd-d1c341ef255b&tag_id=12748f7sdf&tag_status=success"

	**Example response**:

	.. sourcecode:: js

		[{
			"uuid":"a49faabe-d103-462c-97dd-d1c341ef255b",
			"created_by":"u9981",
			"serial_code":"123-test",
			"title":"Koi 1",
			"theme":"Koi",
			"medium":"Canvas",
			"year":"2016",
			"image":"http://www.google.com",
			"width":"1.01m",
			"height":"1.05m",
			"no_of_tags":"1",
			"created_at":"2018-05-22T06:35:00.173902Z"
			"status":"tagged",
			"artist":"a49faabe-d103-462c-97dd-d1c341ef255b"
		},
		{
			"uuid":"a49faabe-d103-462c-97dd-d1c341ef255b",
			"created_by":"u1093",
			"serial_code":"12166-test",
			"title":"Koi 2",
			"theme":"Koi",
			"medium":"Oil on Canvas",
			"year":"2015",
			"image":"http://www.amazon.com",
			"width":"2.01m",
			"height":"1.35m",
			"no_of_tags":"2",
			"created_at":"2018-05-22T06:35:00.173902Z"
			"status":"not tagged",
			"artist":"a49faabe-d103-462c-97dd-d1c341ef255b"
		}
		]

	:>json uuid uuid: UUID of artwork.
	:>json string created_by: User who created the artwork.
	:>json string serial_code: Serial code for artwork.
	:>json string title: Title of artwork.
	:>json string theme: Theme of artwork.
	:>json string medium: Medium of artwork.
	:>json integer year: Year of artwork.
	:>json string image: Image URL of artwork.
	:>json string width: Width of artwork.
	:>json string height: Height of artwork.
	:>json integer no_of_tags: No of tags required for the artwork.
	:>json datetime created_at: Timestamp at which artwork is created.
	:>json string status: Tag status of artwork.
	:>json foreignkey artist: Artist.


	:statuscode 200: OK
	:statuscode 404: No artwork is available




Undocumented resources and endpoints
------------------------------------
There are some undocumented endpoints in the API.
These should not be used and could change at any time.

* Any other endpoints not detailed above.