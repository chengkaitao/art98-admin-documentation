\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Contents}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Artists}{1}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Adding Artists}{1}{subsection.1.1.1}
\contentsline {subsubsection}{Manually}{1}{subsubsection*.3}
\contentsline {subsubsection}{Import with CSV file}{1}{subsubsection*.4}
\contentsline {subsection}{\numberline {1.1.2}Deleting Artists}{1}{subsection.1.1.2}
\contentsline {section}{\numberline {1.2}Artworks}{1}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Adding Artworks}{1}{subsection.1.2.1}
\contentsline {subsubsection}{Manually}{2}{subsubsection*.5}
\contentsline {subsubsection}{Import with CSV file}{2}{subsubsection*.6}
\contentsline {subsection}{\numberline {1.2.2}Deleting Artworks}{2}{subsection.1.2.2}
\contentsline {chapter}{\numberline {2}Indices and tables}{3}{chapter.2}
