
Artworks
========
 
Adding Artworks
---------------
 

Manually
********
1. On the |main_page|, Click on **Artworks** under Backend. 

2. Click **Add Artworks** on the right hand side of the screen.

3. Key in all fields, and click **Save**.

Import with CSV file
********************
1. On the |main_page|, Click on **Artworks** under Backend.
2. Click on **Import** (Next to **Add Artwork**) on the right hand side of the screen.
3. The file format **must be in CSV**. Choose the correct file to import.
4. Click **Submit**.
5. A preview will appear if the import is successful, then click **Import**.


Deleting Artworks
-----------------

1. On the |main_page|, Click on **Artworks** under Backend.
2. On Artworks page, tick the checkbox for the artworks that you would want to delete.
3. Click on the **Action dropdown** located above the artwork list table and pick **Delete Selected Artworks**. Then click **Go**.
4. A confirmation with summary of artists that you want to delete will then pop up. Click **Yes, I'm sure** to permanently delete the artist(s) from the database.

Option to display/remove Artwork on Website
-------------------------------------------

1. On the |main_page|, Click on **Artworks** under Backend.
2. On Artworks page, click on the artwork that you would like to display/remove on website.
3. There will be a 'Display on web' checkbox, untick to not show the artwork on website.


Getting Image Link for Artworks
-------------------------------

1. Head to |imgur_website| and sign in.

.. figure::  images/imgur-add-image-1.png
   :align:   center

2. Click Add image as shown in the screenshot above.

.. figure::  images/imgur-add-image-2.png
   :align:   center

3. Drag all artwork images, or click the Upload button to upload. You are able to upload multiple images at once.

.. figure::  images/imgur-add-image-3.png
   :align:   center

4. You are able to see uploaded images on the main screen when the upload is complete. To get the image link for the admin panel, click on the image.

.. figure::  images/imgur-add-image-4.png
   :align:   center

5. Copy **direct link** and paste it on the Image URL in Admin Panel.

.. |main_page| raw:: html

    <a href="http://art98-staging.herokuapp.com/admin/" target="_blank">Main Page</a>

.. |imgur_website| raw:: html

    <a href="https://imgur.com/signin" target="_blank">imgur.com</a>